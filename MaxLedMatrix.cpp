#include "MaxLedMatrix.h"

MaxLedMatrix::MaxLedMatrix(byte clkPin, byte csPin, byte dinPin, byte chipNumber) {
    CLK_PIN = clkPin;
    CS_PIN = csPin;
    DIN_PIN = dinPin;
    CHIP_NUMBER = chipNumber;

    BUFFER = new byte*[8];

    for (int i = 0; i < 8; i++) {
        BUFFER[i] = new byte[CHIP_NUMBER];
    }
}

/**
 *  +-----------------------+
 *  | INITIALIZATION SECTOR |
 *  +-----------------------+
 *
 */

// Initializer wrapper
void MaxLedMatrix::init() {
    pins_init();

    digitalWrite(CLK_PIN, HIGH);

    buffer_init();
    chips_init();

    setIntensity(0x0F);
}

void MaxLedMatrix::pins_init() {
    pinMode(CLK_PIN,  OUTPUT);
    pinMode(CS_PIN, OUTPUT);
    pinMode(DIN_PIN,  OUTPUT);
}

// Buffer initializer
void MaxLedMatrix::buffer_init() {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < CHIP_NUMBER; j++) {
            BUFFER[i][j] = 0x00;
        }
    }
}



/**
 *  +--------------+
 *  | CHIP CONTROL |
 *  +--------------+
 *
 */

// Chip initializer
void MaxLedMatrix::chips_init() {
    command(MAX7219_REG__SCAN_LIMIT, 0x07);
    command(MAX7219_REG__DECODE_MODE, 0x00);
    command(MAX7219_REG__SHUTDOWN, 0x01);
    command(MAX7219_REG__DISPLAY_TEST, 0x00);
}

void MaxLedMatrix::command(byte registerDescriptor, byte registerValue) {
    digitalWrite(CS_PIN, LOW);
    for (int i = 0; i < CHIP_NUMBER; i++) {
        shiftOut(DIN_PIN, CLK_PIN, MSBFIRST, registerDescriptor);
        shiftOut(DIN_PIN, CLK_PIN, MSBFIRST, registerValue);
    }
    digitalWrite(CS_PIN, LOW);
    digitalWrite(CS_PIN, HIGH);
}

void MaxLedMatrix::setIntensity(byte intensity) {
    command(MAX7219_REG__INTENSITY, intensity);
}

byte MaxLedMatrix::flipByte(byte c) {
    char r = 0;
    for (byte i = 0; i < 8; i++) {
        r <<= 1;
        r |= c & 1;
        c >>= 1;
    }
    return r;
}

/**
 *    +----------------------------+
 *    | REDRAW DISPLAY FROM BUFFER |
 *    +----------------------------+
 *
 */

void MaxLedMatrix::redraw_buffer() {
    for (int row_index = 1; row_index < 9; row_index++) {
        digitalWrite(CS_PIN, LOW);
        draw_rows(row_index);
        digitalWrite(CS_PIN, LOW);
        digitalWrite(CS_PIN, HIGH);
    }
}

void MaxLedMatrix::draw_rows(int row_index) {
    for (int chip_index = 0; chip_index < CHIP_NUMBER; chip_index++) {
        draw_atom_row(chip_index, row_index, BUFFER[row_index-1][chip_index]);
    }
}

void MaxLedMatrix::draw_atom_row(int chip_index, int row_index, byte row_content) {
    shiftOut(DIN_PIN, CLK_PIN, MSBFIRST, row_index);
    shiftOut(DIN_PIN, CLK_PIN, MSBFIRST, row_content);
}

/**
 *    +-----------------+
 *    | SHIFTING BUFFER |
 *    +-----------------+
 *
 */

void MaxLedMatrix::shift_buffer() {
    for (int buffer_row_index = 0; buffer_row_index < 8; buffer_row_index++) {
        shift_buffer_row(buffer_row_index);
    }
}

void MaxLedMatrix::shift_buffer_row(int buffer_row_index) {
    bool rest_values[CHIP_NUMBER];

    for (int flag_index = 0; flag_index < CHIP_NUMBER; flag_index++) {
        rest_values[flag_index] = false;
    }

    for (int row_index = 0; row_index < CHIP_NUMBER; row_index++) {

        // If not last chip - propagate bits
        if (row_index != CHIP_NUMBER - 1) {
            byte rest = BUFFER[buffer_row_index][row_index] & 0x80;
            if (rest != 0x00) {
                rest_values[row_index+1] = true;
            }
        }

        BUFFER[buffer_row_index][row_index] = BUFFER[buffer_row_index][row_index] << 0x01;
        if (rest_values[row_index]) {
            BUFFER[buffer_row_index][row_index] = BUFFER[buffer_row_index][row_index] | 0x01;
            rest_values[row_index] = false;
        }
    }
}

// --------------------------------

void MaxLedMatrix::slideText(char text[16], int delayTime) {
    for (int character_index = 0; character_index < 16; character_index++) {
        char current_character = text[character_index];

        for (int row_index = 0; row_index < 8; row_index++) {
            BUFFER[row_index][0] = charToByteArrayDisplay(current_character, row_index);
        }

        for (int i = 0; i < 8; i++) {
            redraw_buffer();
            shift_buffer();
            delay(delayTime);
        }
    }
}