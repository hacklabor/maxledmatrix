#include "MaxLedMatrix.h"

MaxLedMatrix maxLedMatrix(10, 11, 12, 16);

void setup()
{
    maxLedMatrix.init();
    maxLedMatrix.setIntensity(0x00);
}

void loop()
{
    char message[16] = "hacklabor  123  ";
    maxLedMatrix.slideText(message, 50);
}