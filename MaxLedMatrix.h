#ifndef MAXLEDMATRIX_MAXLEDMATRIX_H
#define MAXLEDMATRIX_MAXLEDMATRIX_H

#include "Arduino.h"
#include "FontAssets.h"
#include "Max7219.h"

class MaxLedMatrix
{
public:
    byte CLK_PIN;
    byte CS_PIN;
    byte DIN_PIN;
    byte CHIP_NUMBER;
    byte **BUFFER;

    void pins_init();
    void buffer_init();
    byte flipByte(byte);

    void clear();

    MaxLedMatrix(byte clkPin, byte csPin, byte dinPin, byte chipNumber);

    void slideText(char[16], int);
    void init();
    void setIntensity(byte);
    void command(byte, byte);
    void chips_init();
    void redraw_buffer();
    void draw_rows(int);
    void draw_atom_row(int, int, byte);
    void shift_buffer();
    void shift_buffer_row(int);
};

#endif //MAXLEDMATRIX_MAXLEDMATRIX_H
