#include "FontAssets.h"

byte charToByteArrayDisplay(char character, int row) {
    int asciiValue = (int) character;
    if (asciiValue > 47 && asciiValue < 58)
        return charToDigitByteArray(character, row);
    else
        return charToLetterByteArray(character, row);
}

byte charToDigitByteArray(char character, int row) {
    return DIGITS[character - '0'][row];
}

byte charToLetterByteArray(char character, int row) {
    // CHARACTER VALUE  | ASCII VALUE   | LETTERS VALUE
    // A-Z              | 65 - 90       | 0 - 25
    // a-z              | 97 - 122      | 27 - 52
    // space            | 32            | 26

    int asciiValue = (int) character;

    // Handle space
    if (asciiValue == 32) return LETTERS[26][row];

    // Handle capital letters
    if (asciiValue > 64 && asciiValue < 91) {
        asciiValue -= 65;
        return LETTERS[asciiValue][row];
    }

    // Handle small letters
    if (asciiValue > 96 && asciiValue < 123) {
        asciiValue -= 97;
        return LETTERS[asciiValue + 27][row];
    }

    return 0x00;
}